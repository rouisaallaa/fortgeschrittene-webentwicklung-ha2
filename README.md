kleine Anmerkung: Ich habe letztes Semester Testat bekommen
### Tech

Unser Projekt verwendet eine Reihe von Bibliotheken und Technologien:

* MySQL - database management system
* node.js - evented I/O for the backend
* npm - package manager
* "react": "^16.13.1",
* "react-dom": "^16.13.1",
* "react-router-dom": "^5.2.0",
* "react-router-redux": "^4.0.8",
* "react-scripts": "^3.4.1",
* "styled-components": "^5.1.0"

### Installation and Configuration

1. Klonen Sie das Repository FWE-WS-20-21-757851-HA2 auf Ihren lokalen Computer

#### Backend
Erstellen Sie das Projekt und starten Sie den Backend-Server, indem Sie die Schritte in der Dokumentation **https://code.fbi.h-da.de/istalroui/fwe-ws20-21-757851-ha1/-/blob/master/README.md** ausführen

#### Frontend
Installieren die dependencies und devDependencies und starten den Server:
```sh
$ cd FWE-SS20-757851-HA2
$ npm install
$ npm start
```
Die Anwendung ist unter http://localhost:3000 verfügbar

### How To Use:

##### Dashboard
Die Seite ist unter http://localhost:3000 verfügbar
![dashboard](docs/dashboard.png)
1. Bearbeiten das Task.
2. Löschen ein Task..
3. Fügen ein neues Task hinzu. 
4. Filtern die Task nach Name
5. Filtern die Task nach Description
6. Filtern die Task nach Label

##### Recipe Details Page
Die Seite ist unter http://localhost:3000/task/{taskId} verfügbar

![Task](docs/Task.png)

1. Task bearbeiten
2. Fügen eine Tracking aus einem Task hinzu
3. Entfernen eine Tracking aus einem Task
4. Fügen eine Label aus einem Task hinzu
5. Entfernen eine Label aus einem Task
6. Fügen eine Label hinzu
7. Der Benutzer wird zu Seite dashboard weitergeleitet