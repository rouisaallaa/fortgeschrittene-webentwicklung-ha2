import React from "react" ;
import styled, {css} from "styled-components";
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import { DashboardPage } from "../pages/Dashboard/DashboardPage" ;
import { TaskPage } from "../pages/Task/TaskPage" ;
import { TaskCreatePage } from "../pages/Task/TaskCreatePage" ;
import { LabelCreatePage } from "../pages/Label/LabelCreatePage" ;

export const Layout:React.FC = ({children}) => {
	const headerHeight = "85px";
	const footerHeight = "50px";

	const MaxWidthCSS = css`
		max-width: 860px;
		margin: auto;
	`;
	const Header = styled.header`
		height: ${headerHeight} ;
		width: 100%;
		display: flex;
		align-item: center;
		padding: 0 25px;
	`;
	const Main = styled.main`
		min-height: calc(100vh - ${headerHeight} - ${footerHeight});
		${MaxWidthCSS}
	`;
	const Footer = styled.footer`
		height: ${footerHeight} ;
		${MaxWidthCSS}
	`;
	const NavigationList = styled.ul` list-style: none;` ; 
	const NavigationItem = styled.li` color: ${props => props.theme.colors.primary}; ` ;

	const Span1 = styled.span`
	    font-size: 1.1rem;
	    margin-left: 25px;
	    color: red;
	`;
	return(
		<><Router>
			<Header>
				<NavigationList>
					<NavigationItem>
						<Link to={'/'} className="nav-link"><Span1> Dashboard </Span1> </Link>
						<Link to={'/labelCreate'} className="nav-link"><Span1> Create Label </Span1> </Link>
					</NavigationItem>
				</NavigationList>

			</Header>
			<Main>
				<Switch>
				    <Route exact path='/' component={DashboardPage} />     
				    <Route exact path='/task/:TaskId' component={TaskPage} />     
				    <Route exact path='/TaskCreate' component={TaskCreatePage} />     
				    <Route exact path='/labelCreate' component={LabelCreatePage} />     
				</Switch>
			</Main>
			<Footer></Footer>
		</Router>
		</> 
	);
}
