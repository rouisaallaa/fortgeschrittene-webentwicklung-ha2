import React, { useState, useEffect }  from "react" ;
import { useHistory } from "react-router-dom";
import styled from "styled-components";
import { TaskList, TaskItem, AddButton} from "./components/TaskList";


const Div1 = styled.div`
	display: flex;
	flex-direction: row;
	width: 100%;
`;

const Div2 = styled.div`
	flex: 1;
	justify-content: flex-end;
	display: flex;
	align-items: top;
`;


export const DashboardPage = () => {
	const [tasks, settasks] = useState<any[]>([]);
	const [tasks2, settasks2] = useState<any[]>([]);
	const [labels, setlabels] = useState<any>();

	const history = useHistory();
	const rating = [0,1,2,3,4,5,6,7,8,9,10];
	useEffect(() => {
	    (async () => {
		    const request = await fetch("http://localhost:3001/api/task")
		    const json = await request.json();
		    settasks(json.data);
		    settasks2(json.data) ;
	     })() ; 

	    (async () => {
	    	const request = await fetch(`http://localhost:3001/api/label`);
	    	const json = await request.json();
	    	console.log(json);
	    	setlabels(json.data);
	    })();

  	},[]);

	const CreateTask = (e :any  ) => {
         history.push("/TaskCreate")
    };



    const FilterByName = (e :any  ) => {
    	let Value = e.target.value ;
    	console.log(Value);
    	if(Value !== "-"){
	    	let task2 : any[] = [] ;
	    	tasks2.map( (task , i) => {
	    		if(task.name == Value){
	    			task2.push(task);
	    		}
			});
		    settasks(task2);
		}else{
			settasks(tasks2);
		}

    };

    const FilterByDescription = (e :any  ) => {
    	let Value = e.target.value ;
    	if(Value !== "-"){
	    	let task2 : any[] = [] ;
	    	tasks2.map( (task , i) => {
	    		if(task.description == Value){
	    			task2.push(task);
	    		}
			});
		    settasks(task2);
		}else{
			settasks(tasks2);
		}

    };
	const FilterByLabel = (e :any  ) => {
    	let Value = e.target.value ;
    	if(Value !== "-"){
	    	let task2 : any[] = [] ;
	    	tasks2.map( (task , i) => {
	    		console.log(task.labels);
	    		task.labels.map( (label , i ) => {
	    			if(label.LabelName == Value){
	    				task2.push(task);
	    			}
	    		});
	    		
			});
		    settasks(task2);
		}else{
			settasks(tasks2);
		}
    };

	return ( 
		<div>
			<Div1>
				<Div2>	
					<AddButton onClick={(e) => {CreateTask(e)}} />
					
					<select name="name" id="name" onChange={FilterByName}>
						<option  value="-">- Name -</option>
						{tasks2?.map( (task , i) => {
							return ( <option key={i} value={task.name}>{task.name}</option> );
						})}
					</select>
					<select name="description" id="description" onChange={FilterByDescription}>
						<option  value="-">- Description -</option>
						{tasks2?.map( (task , i) => {
							return ( <option key={i} value={task.description}>{task.description}</option> );
						})}
					</select>
					<select name="label" id="label" onChange={FilterByLabel}>
						<option  value="-">- Label -</option>
						{labels?.map( (label , i) => {
							return ( <option key={i} value={label.name}>{label.name}</option> );
						})}
					</select>  
				</Div2>	
			</Div1>
			<TaskList>
				{tasks?.map( (task , i) => {
					return (<TaskItem key={i} task={tasks[i]}> </TaskItem>);
				})} 
			</TaskList>
			
		</div>	
		) ; 
} 