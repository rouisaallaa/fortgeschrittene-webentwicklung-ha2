import React from "react";
import styled from "styled-components";
import { useHistory } from "react-router-dom";




export const TaskList = styled.ul`
    list-style: none;
    box-shadow: 0 0.125em 0.25 0 &{(props) => props.theme.colors.shadowColor};
    width: 100%;
    padding: 0;
    border-radius: 0.5rem;
    background-color: &{(props) => props.theme.colors.listBackgroundColor};
    &(RecipeItemStyle){
        border-bottom: 1px &{(props) => props.theme.colors.shadowColor};
        &:last-of-type {
            border-bottom: 0;
        }
    }
`;
export const TaskItemStyle = styled.div`
    margin: 0;
    min-height: 3rem;
    position: relative;
    padding: 0.7rem 2rem;
    &:hover{
        color: #000;
        background-color: #fff;
    }
`;

export const TaskTitle = styled.p`
    font-size: 1.1rem;
    font-weight: 500;
    margin; 0;
`;
export type TaskItemProps = {
    task : any;
};

export const DeleteButton = styled.button`
  background-color: white; 
  color: black; 
  border: 2px solid #f44336;
  font-size: 24px;
  position: absolute;
  right: 0;
  top: 50px;
  &:hover{
    background-color: #f44336;
    color: white;
  }
`;
export const TaskDescription = styled.p`
    font-size: 0.8rem;
    margin: 0;
`;

export const DetailButton = styled.button`
  background-color: white; 
  color: black; 
  border: 2px solid #f44336;
  font-size: 24px;
  position: absolute;
  right: 0;
  top: 5px;
  &:hover{
    background-color: #f44336;
    color: white;
  }
`;
export const AddButton = (props: React.ButtonHTMLAttributes<HTMLButtonElement>)=> {
  const StyledButton = styled.button`
    width: 100px;
    border: 0px;
    height: 48px;

    background-color: ${(props) => props.theme.colors.primary};
  `;

  return (
    <StyledButton {...props} >create Task
    </StyledButton>
    )
  
};

export const TaskItem : React.FC<TaskItemProps> = ({
    task: {name ,id ,description ,labels ,tracking , updatedAt, createdAt} ,
}) => {
  const history = useHistory();
    const getTask = (e :any , id : any ) => {
     history.push("/task/"+id);
    }
    const DeleteEven = (e :any , id : any ) => {  
      (async () => {
        const request = await fetch("http://localhost:3001/api/task/"+ id , {method: 'Delete'});
        await request.json();
        window.location.reload(false);
      })();
      
      };
    
    return (
       <TaskItemStyle>
            <TaskTitle>Id-{id} {name}</TaskTitle>
            <TaskDescription>{description}</TaskDescription>
            <DetailButton onClick={(e) => {getTask(e, id)}}>Detail & Update Task</DetailButton>
            <DeleteButton onClick={(e) => {DeleteEven(e, id)}}>Delete Task</DeleteButton>
       </TaskItemStyle>
    );
};

