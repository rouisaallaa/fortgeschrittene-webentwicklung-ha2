import React  from "react" ;
import { useHistory } from "react-router-dom";

export const LabelCreatePage = (props : any) => {
  const history = useHistory();
  var labelUpdate : object = {} ;

  const submitLabel = (e :any ) => {
    e.preventDefault();
    (async () => {
      const request = await fetch(`http://localhost:3001/api/label` ,{
       method : 'POST',
       headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
       body:  JSON.stringify(labelUpdate)
     });

    const json = await request.json();
    alert("successfully");
    history.push("/")
    
  })()
      
  };

  const handleChange = (e :any , index : string) => {
      labelUpdate[index] = e.target.value ;
      console.log(index);
      if(labelUpdate[index].length === 0){
        delete labelUpdate[index] ;
      }

  };

  return ( 
    <>
      <form onSubmit={submitLabel}>
          <label>
            Nom : 
            <input type="text"  onChange={(e) => { handleChange(e , "name") } } />
          </label>

          <br />
          <br />
          <input type="submit" value="send" />
        </form>
    </>    
    ) ; 
} 
