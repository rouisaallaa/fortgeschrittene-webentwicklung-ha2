import React  from "react" ;
import { useHistory } from "react-router-dom";

export const TaskCreatePage = (props : any) => {
  const history = useHistory();
  var taskUpdate : object = {} ;

  const submitTask = (e :any ) => {
    e.preventDefault();
    (async () => {
      const request = await fetch(`http://localhost:3001/api/task` ,{
       method : 'POST',
       headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
       body:  JSON.stringify(taskUpdate)
     });
    alert("successfully");
    history.push("/")
    
  })()
      
  };

  const handleChange = (e :any , index : string) => {
      taskUpdate[index] = e.target.value ;
      console.log(index);
      if(taskUpdate[index].length === 0){
        delete taskUpdate[index] ;
      }else if(index === "rating"){
        taskUpdate[index] = +e.target.value
      }

  };

  return ( 
    <>
      <form onSubmit={submitTask}>
          <label>
            Nom : 
            <input type="text"  onChange={(e) => { handleChange(e , "name") } } />
          </label>
          <br />
          <br />
          <label>
            description : 
            <input type="text"  onChange={(e) => { handleChange(e , "description") } } />
          </label>
          <br />
          <br />
          <input type="submit" value="send" />
        </form>
    </>    
    ) ; 
} 
