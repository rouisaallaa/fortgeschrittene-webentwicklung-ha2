import React, { useState, useEffect }  from "react" ;
import { useHistory } from "react-router-dom";
import styled from "styled-components";


export const Button = styled.button`
  width: 20px;
  border: 0px;
  height: 20px;
  border-radius: 50%;
  background-color: ${(props) => props.theme.colors.primary};
`;

export const Span1 = styled.span`
    font-size: 1.1rem;
    color: red;
    
`;
export const AddButton = (props: React.ButtonHTMLAttributes<HTMLButtonElement>)=> {
  const StyledButton = styled.button`
    width: 100px;
    border: 0px;
    height: 48px;
  `;

  return (
    <StyledButton {...props} >Add Tracking To Task
    </StyledButton>
    )
  
};
export const TaskPage = (props : any) => {
  const history = useHistory();
  const [task, settask] = useState<any>();
  const [Label, setLabel] = useState<any>();
  const [Tracking, setTracking] = useState<any>();
  var taskUpdate : object = {} ;
  var createTrackingg : object = {} ;

  const [editButton, seteditButton] = useState<boolean>(false);
  const [trackingCreateButton, settrackingCreateButton] = useState<boolean>(false);

  const submitTask = (e :any ) => {
    (async () => {
      const request = await fetch(`http://localhost:3001/api/task/${props.match.params.TaskId}` ,{
       method : 'PATCH',
       headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
       body:  JSON.stringify(taskUpdate)
     });

    const json = await request.json();

    if(json.status === "not_found"){
      alert(json.status);
    }else{
       history.push("/")
    }
  })()
       e.preventDefault();
    
  };

  const submitTracking = (e :any ) => {
    e.preventDefault();
    (async () => {
      const request = await fetch(`http://localhost:3001/api/tracking/task/${props.match.params.TaskId}` ,{
       method : 'POST',
       headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
       body:  JSON.stringify(createTrackingg)
     });
    alert("successfully");
    window.location.reload(false);
  })()
      
  };

  const handleChange = (e :any , index : string) => {
      taskUpdate[index] = e.target.value ;
      if(taskUpdate[index].length === 0){
        delete taskUpdate[index] ;
      }
    
  };

  const handleChangetracking = (e :any , index : string) => {
      createTrackingg[index] = e.target.value ;
      if(createTrackingg[index].length === 0){
        delete createTrackingg[index] ;
      }
    
  };

  const createTracking = (e :any , index : string) => {
      settrackingCreateButton(!trackingCreateButton)
       e.preventDefault();
  };

  const EditTask = (e :any , index : string) => {

      seteditButton(!editButton)
       e.preventDefault();
  };

    const DeleteLabelFromTask = (e :any , labelId : string) => {
     (async () => {
        const request = await fetch("http://localhost:3001/api/taskLabel/label/"+ labelId +"/task/"+props.match.params.TaskId  , {method: 'Delete'});
        window.location.reload(false);
      })()
  };

  const DeleteTrackingFromTask = (e :any , Tracking : string) => {
     (async () => {
        const request = await fetch("http://localhost:3001/api/tracking/"+ Tracking  , {method: 'Delete'});

        window.location.reload(false);
      })()
  };

  const addLabelToTask = (e :any , labelId : string) => {

     (async () => {

      const request = await fetch("http://localhost:3001/api/taskLabel/label/"+ labelId +"/task/"+props.match.params.TaskId ,{
       method : 'POST',
       headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
       body:  JSON.stringify({ })
      });
      window.location.reload(false);
    
      })()
    
  };


  useEffect(() => {
    (async () => {
      const request = await fetch(`http://localhost:3001/api/task/${props.match.params.TaskId}`);
      const json = await request.json();
      setTracking(json.data.tracking)
      console.log(json.data.tracking)
      settask(json.data);
    })();

    (async () => {
      const request = await fetch(`http://localhost:3001/api/label`);
      const json = await request.json();
      setLabel(json.data);
    })();


    

  },[]);
  return ( 
    <>

    <form onSubmit={submitTask}>
        <label>
          Nom : {!editButton && <Span1>{task?.name}</Span1> }
          {editButton && <input type="text"  onChange={(e) => { handleChange(e , "name") } } /> }
        </label>
        <br />
        <br />
        <label>
          CookingInstructions :  {!editButton && <Span1>{task?.description} </Span1>}
          {editButton && <input type="text"   onChange={(e) => { handleChange(e , "description") } } />}
        </label>
        <br /><br />
        {editButton && <input type="submit" value="send" /> }
        {!editButton && <button onClick={(e) => { EditTask(e , " " ) } } >Edit</button>}
        {editButton && <button onClick={(e) => { EditTask(e , " " ) } } >cancel</button>}
        <br />
        <br />

      <p> Labels </p>

        <table >
          <thead>
            <tr>
              <th>id</th>
              <th>name</th>
              <th>Delete Label</th>
              
            </tr>
            </thead>
          {task?.labels?.map( (label , i) => {
            return (
              <tbody  key={i}>
                <tr >
                  <td>{label.labelId}</td>
                  <td>{label.LabelName}</td>
                  <td> <Button onClick={(e) => { DeleteLabelFromTask(e , label.labelId ) } }>-</Button> </td>
                </tr>
              </tbody>
              );
          })}
        </table><br /><br /> 

      <p> Tracking </p>
        <table >
          <thead>
            <tr>
              <th>id</th>
              <th>description</th>
              <th>startTime</th>
              <th>endTime</th>
              <th>Delete Tracking From Task </th>
            </tr>
            </thead>
          {Tracking?.map( (tracking , i) => {
            return (
              <tbody  key={i}>
                <tr >
                  <td>{tracking.id}</td>
                  <td>{tracking.description}</td>
                  <td>{tracking.startTime}</td>
                  <td>{tracking.endTime}</td>
                  <td><Button onClick={(e) => { DeleteTrackingFromTask(e , tracking.id ) } }>-</Button> </td>
                </tr>
              </tbody>
              );
          })}
        </table> 
        <br />
      </form>
      <AddButton onClick={ (e) => { createTracking(e , " " )} } ></AddButton>
      <form onSubmit={submitTracking}>
      {trackingCreateButton &&
        <><br />
          description : <input type="text"  onChange={(e) => { handleChangetracking(e , "description") } } /> <br />
          startTime :<input type="text"  onChange={(e) => { handleChangetracking(e , "startTime") } } /> <br />
          endTime : <input type="text"  onChange={(e) => { handleChangetracking(e , "endTime") } } /> <br />  
          <input type="submit" value="send" />
          <button onClick={(e) => { createTracking(e , " " ) } } >cancel</button>
        </>
      }
      </form>
        <br />
        <br />
      <br /><br />
      <p>Add Labels to the Task</p>
      <table >
          <thead>
            <tr>
              <th>id</th>
              <th>name</th>
              <th>createdAt</th>
              <th>updatedAt</th>
              <th>Add Label</th>
            </tr>
            </thead>
          {Label?.map( (label , i) => {
            return (
              <tbody  key={i}>
                <tr >
                <td>{label.id}</td>
                <td>{label.name}</td>
                <td>{label.createdAt}</td>
                <td>{label.updatedAt}</td>
                <td> <Button onClick={(e) => { addLabelToTask(e , label.id ) } }>+</Button> </td>
                </tr>
              </tbody>
              );
          })}
        </table>
      </>
    ) ; 
} 
