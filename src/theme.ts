import { DefaultTheme } from "styled-components" ;

export const theme: DefaultTheme = {
	colors: {
			primary: "rgb(54 , 161 , 139)",
			backgroundColor: "#202020",
			fontColor: "#fff",
			secondaryFontColor:  "rgb(191, 191, 191)",
			shadowColor: "rgb( 0, 0, 0, 0.3)",
			listBackgroundColor: "rgb(45, 45, 45)",
			
		},
};